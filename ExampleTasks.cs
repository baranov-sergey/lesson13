﻿using Lesson13.Task1;
using Lesson13.Task2;

namespace Lesson13
{
    internal class ExampleTasks
    {
        public static void App()
        {
            /*        
                Реализовать следующие методы:
                - Метод возвращает первое слово из последовательности слов, содержащее только одну букву.
                - Метод, возвращающий последнее слово, содержащее в себе подстроку «ее».
                Реализовать, используя только 1 метод LINQ.
                - Реализовать метод для возврата последнего слова, соответствующего условию:
                длина слова не меньше min и не больше max. Если нет слов, соответствующих условию, метод возвращает null.
                - Напишите метод, который возвращает количество уникальных значений в массиве.
                - Метод возвращает длину самого короткого слова
                - Напишите метод, который принимает список и извлекает значения с 5 элемента (включительно)
                те значение которые содержат 3.
                - Напишите метод, который преобразует словарь в список и меняет местами каждый ключ и значение.
            */

            string str = "Радости этой жизни суть не ее радости, а наш страх пред восхождением в высшую жизнь;" +
                " муки этой жизни суть не ее муки, а наше самобичевание из-за этого страха.";
            var list = new List<string>(str.Split());
            Tasks.Task1(list);
            Tasks.Task2(list);
            Tasks.Task3(list);
            Tasks.Task4(list);
            Tasks.Task5(list);
            Tasks.Task6(list);

            var dic = new Dictionary<int, string>()
            {
                { 1,"one" },
                { 2,"two" },
                { 3,"three" },
                { 4,"four" },
                { 5,"five"},
                { 6,"six"}
            };
            Tasks.Task7(dic);

            /*        
                дан класс
                public class User
                {
                public string FirstName {get; set; }
                public string MiddleName { get; set; }
                public string LastName {get; set; }
                public User(string firstName, string middleName, string lastName)
                {
                FirstName = firstName;
                MiddleName = middleName;
                LastName = lastName;
                }
                }

                - Напишите метод, который возвращает "<FirstName> <MiddleName> <LastName>", если отчество присутствует Или "<FirstName>
                <LastName>", если отчество отсутствует.

                -Напишите метод, который возвращает предоставленный список пользователей, упорядоченный по LastName в порядке убывания.
            */

            var users = new UserCollection<User>();
            users.Add(new User("Stepan", "Sergeevich", "Ivanov"))
                .Add(new User("Ivan", "Ivanovich", "Xoroshilov"))
                .Add(new User("Alena", "Vasilevna", "Orexova"))
                .Add(new User("Oleg", "Filippovich", "Fedorov"))
                .Add(new User("Pavel", "", "Morozov"))
                .Add(new User("Nikita", "Aleksandrovich", "Volkov"))
                .Add(new User("Roman", "Arturovich", "Alekseev"))
                .Add(new User("Stanislav", null, "Lebedev"))
                .Add(new User("Timur", "Danilovich", "Semenov"))
                .Add(new User("Kirill", "Eduardovich", "Orlov"));

            //-Напишите метод, который возвращает предоставленный список пользователей,
            //упорядоченный по LastName в порядке убывания.
            var arrayindescendingorder = users.OrderByDescending(x => x.LastName);
            arrayindescendingorder.ToList().ForEach(u => Console.WriteLine(u));
        }
    }
}
