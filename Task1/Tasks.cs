﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson13.Task1
{
    public class Tasks
    {
        public static void Task1(List<string> list)
        {
            //- Метод возвращает первое слово из последовательности слов, содержащее только одну букву.
            var firstWordOneLetter = list.First(w => w.Length == 1);
            Console.WriteLine(firstWordOneLetter);
        }

        public static void Task2(List<string> list)
        {
            //- Метод, возвращающий последнее слово, содержащее в себе подстроку «ее». Реализовать, используя только 1 метод LINQ.
            var lastWordEe = list.Last(w => w.Contains("ее"));
            Console.WriteLine(lastWordEe);
        }

        public static void Task3(List<string> list)
        {
            //- Реализовать метод для возврата последнего слова, соответствующего условию: длина слова не меньше min и не больше max.Если нет слов,
            //соответствующих условию, метод возвращает null.
            int min = 3, max = 5;
            var lastWordRangeMinMax = list.Last(w => w.Length >= min && w.Length <= max);
            Console.WriteLine(lastWordRangeMinMax);
        }

        public static void Task4(List<string> list)
        {
            //- Напишите метод, который возвращает количество уникальных значений в массиве.
            var uniqueValues = list.Distinct().Count();
            Console.WriteLine(uniqueValues);
        }

        public static void Task5(List<string> list)
        {
            //- Метод возвращает длину самого короткого слова
            var shortWord = list.OrderBy(w => w.Length).First().Count();
            Console.WriteLine(shortWord);
        }

        public static void Task6(List<string> list)
        {
            //- Напишите метод, который принимает список и извлекает значения с 5 элемента (включительно) те значение которые содержат 3.
            var task6 = list.Skip(4).Where(w => w.Contains('3')).ToList();
            task6.ForEach(w => Console.WriteLine(w));
        }

        public static void Task7<TKey, TValue>(Dictionary<TKey, TValue> dic)
        {
            //- Напишите метод, который преобразует словарь в список и меняет местами каждый ключ и значение
            var list = dic.ToDictionary(k => k.Value, v => v.Key).ToList();
            list.ForEach(w => Console.WriteLine(w));
        }
    }
}