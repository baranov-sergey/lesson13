using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson13.Task2
{
    internal class User
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public User(string firstName, string middleName, string lastName)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
        }

        //Напишите метод, который возвращает "<FirstName> <MiddleName> <LastName>", если отчество присутствует Или "<FirstName>
        //<LastName>", если отчество отсутствует.
        public override string ToString()
        {
            return MiddleName == null || MiddleName == string.Empty ? $"FirstName: {FirstName}, LastName: {LastName}"
                : $"FirstName: {FirstName}, MiddleName: {MiddleName}, LastName: {LastName}";
        }
    }
}
