﻿using System.Collections;
using Lesson13.Task1;

namespace Lesson13.Task2
{
    internal class UserCollection<T> : IEnumerable<T>
    {
        private T[] users;
        private const int size = 10;
        private int cnt = -1;

        public UserCollection() => users = new T[size];

        public UserCollection<T> Add(T user)
        {
            cnt++;
            if (cnt > users.Length - 1)
            {
                Array.Resize(ref users, users.Length + size);
            }

            users[cnt] = user;
            return this;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= users.Length) throw new IndexOutOfRangeException();
            for (int i = index; i < users.Length - 1; i++)
            {
                users[i] = users[i + 1];
            }
            Array.Resize(ref users, users.Length - 1);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new UserCollectionEnumerator<T>(users);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public T this[int index]
        {
            get => users[index];
            set => users[index] = value;
        }
    }
}
