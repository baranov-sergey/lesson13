﻿using System.Collections;

namespace Lesson13.Task2
{
    internal class UserCollectionEnumerator<T> : IEnumerator<T>
    {
        private T[] users;
        private int index = -1;
        public T Current { get => users[index]; }
        public UserCollectionEnumerator(T[] users) => this.users = users;

        object IEnumerator.Current => throw new NotImplementedException();

        public void Dispose() { }

        public bool MoveNext()
        {
            if (index < users.Length - 1)
            {
                index++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
